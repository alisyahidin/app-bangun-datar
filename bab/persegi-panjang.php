<p class="materi-definisi">Persegi panjang adalah bangun datar yang memiliki sisi berhadapan sama panjang dan memiliki empat titik sudut. Selain memiliki sisi yang berhadapan sama panjang dan empat titik sudut yang sama besar yaitu 90°.</p>

<div class="materi-sifat">
    <ol class="materi-sifat">
        <h4>Sifat</h4>
        <li>Memiliki empat dua diagonal yang sama panjang</li>
        <li>Memliki 3 simetri lipat</li>
        <li>Memliki simetri putar tingkat dua</li>
    </ol>
</div>

<div class="materi-rumus">
    <blockquote class="materi-rumus-luas">
        <h4>Rumus Luas</h4>
        <p>L = P x L</p>
    </blockquote>
    <blockquote class="materi-rumus-keliling">
        <h4>Rumus Keliling </h4>
        <p>K = 2 x (P + L)</p>
    </blockquote>
</div>

<div class="materi-menghitung">
    <h4>Menghitung Luas dan Keliling</h4>
    <label class="persegi-panjang" for="panjang">Masukkan panjang
        <input id="panjang" type="number">
    </label>
    <label class="persegi-panjang" for="lebar">Masukkan lebar
        <input id="lebar" type="number">
    </label>
    <button type="submit" name="hitung-luas" onclick="luas()">Hitung Luas</button>
    <button type="submit" name="hitung-keliling" onclick="keliling()" >Hitung Keliling</button>
</div>

<div class="materi-hasil">
    <p><b>Luas</b></p>
    <input type="text" id="materi-hasil-luas" readonly="readonly">
    <p><b>Keliling</b></p>
    <input type="text" id="materi-hasil-keliling" readonly="readonly">
</div>

<script type="text/javascript">
    function luas() {
        var result = document.getElementById('materi-hasil-luas')
        var pj = new Number(document.getElementById('panjang').value)
        var lb = new Number(document.getElementById('lebar').value)
        var hasil = pj*lb
        result.value = hasil
    }
    function keliling() {
        var result = document.getElementById('materi-hasil-keliling')
        var pj = new Number(document.getElementById('panjang').value)
        var lb = new Number(document.getElementById('lebar').value)
        var hasil = 2 * (pj + lb)
        result.value = hasil
    }
</script>