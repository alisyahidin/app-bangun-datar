<p class="materi-definisi">Segitiga atau segi tiga adalah bangun datar yang dibentuk oleh tiga sisi berupa garis lurus dan memiliki tiga sudut. Selain memiliki 3 sisi atau rusuk dan sudut, sifat yang dimiliki segitiga memiliki besar sudut 180°.</p>

<div class="materi-sifat">
    <ol class="materi-sifat">
        <h4>Sifat</h4>
        <li>Segitiga sama sisi (bahasa Inggris: equilateral triangle) adalah segitiga yang ketiga sisinya sama panjang dan semua sudutnya sama besar yaitu 60°.</li>
        <li>Segitiga sama kaki (bahasa Inggris: isoceles triangle) adalah segitiga yang dua dari tiga sisinya sama panjang dan memiliki dua sudut yang sama besar.</li>
        <li>Segitiga sembarang (bahasa Inggris: scalene triangle) adalah segitiga yang ketiga sisi memiliki panjang dan besar semua sudutnya berbeda.</li>
    </ol>
</div>

<div class="materi-rumus">
    <blockquote class="materi-rumus-luas">
        <h4>Rumus Luas</h4>
        <p>L = 1/2 x A x T</p>
    </blockquote>
    <blockquote class="materi-rumus-keliling">
        <h4>Rumus Keliling </h4>
        <p>K = S + S + S</p>
    </blockquote>
</div>

<div class="materi-menghitung">
    <h4>Menghitung Luas dan Keliling</h4>
    <label for="alas">Silahkan masukkan panjang alas
        <input id="alas" type="number">
    </label>
    <label for="tinggi">Silahkan masukkan tinggi
        <input id="tinggi" type="number">
    </label>
    <button type="submit" name="hitung-luas" onclick="luas()">Hitung Luas</button>

    <label for="sisi">Silahkan masukkan panjang sisi
        <input id="sisi" type="number">
    </label>
    <button type="submit" name="hitung-keliling" onclick="keliling()" >Hitung Keliling</button>
</div>  

<div class="materi-hasil">
    <p><b>Luas</b></p>
    <p id="materi-hasil-luas"></p>
    <p><b>Keliling</b></p>
    <p id="materi-hasil-keliling"></p>
</div>

<script type="text/javascript">
    function luas() {
        var result = document.getElementById('materi-hasil-luas')
        var alas = new Number(document.getElementById('alas').value)
        var tinggi = new Number(document.getElementById('tinggi').value)
        var hasil = 1/2 * alas * tinggi
        result.innerHTML = "Luas : " + hasil
    }
    function keliling() {
        var result = document.getElementById('materi-hasil-keliling')
        var sisi = new Number(document.getElementById('sisi').value)
        var hasil = sisi + sisi + sisi
        result.innerHTML = "Keliling : " + hasil
    }
</script>