<p class="materi-definisi">Lingkaran adalah himpunan semua titik pada bidang dalam jarak tertentu yang disebut dengan jari-jari dari seatu titik yang disebut dengan pusat.</p>

<div class="materi-sifat">
    <ul class="materi-sifat">
        <h4>Sifat</h4>
        <li>Mempunyai simetri putar tak terhingga.</li>
        <li>Mempunyai simetri lipat serta sumbu yang tak terhingga.</li>
        <li>Tidak memiliki titik sudut.</li>
        <li>Memiliki satu sisi.</li>
    </ul>
</div>

<div class="materi-rumus">
    <blockquote class="materi-rumus-luas">
        <h4>Rumus Luas</h4>
        <p>L = &pi; x R<sup>2</sup></p>
    </blockquote>
    <blockquote class="materi-rumus-keliling">
        <h4>Rumus Keliling </h4>
        <p>K = &pi; x D</p>
    </blockquote>
</div>

<div class="materi-menghitung">
    <h4>Menghitung Luas dan Keliling</h4>
    <label for="jari">Silahkan masukkan panjang jari-jari
        <input id="jari" type="number">
    </label>
    <button type="submit" name="hitung-luas" onclick="luas()">Hitung Luas</button>
    <button type="submit" name="hitung-keliling" onclick="keliling()" >Hitung Keliling</button>
</div>

<div class="materi-hasil">
    <p><b>Luas</b></p>
    <p id="materi-hasil-luas"></p>

    <p><b>Keliling</b></p>
    <p id="materi-hasil-keliling"></p>
    
</div>

<script type="text/javascript">
    function luas() {
        var result = document.getElementById('materi-hasil-luas')
        var jari = new Number(document.getElementById('jari').value)
        var hasil = 3.14 * (jari*jari)
        result.innerHTML = "Luas : " + hasil
    }
    function keliling() {
        var result = document.getElementById('materi-hasil-keliling')
        var jari = new Number(document.getElementById('jari').value)
        var hasil = 3.14 * (jari+jari)
        result.innerHTML = "Keliling : " + hasil
    }
</script>