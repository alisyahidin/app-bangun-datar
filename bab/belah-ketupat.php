<p class="materi-definisi">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

<ul class="materi-sifat">
    <h4>Sifat</h4>
    <li>sifat pertama</li>
    <li>sifat kedua</li>
    <li>sifat ketiga</li>
</ul>

<div class="materi-rumus">
    <blockquote class="materi-rumus-luas">
        <h4>Rumus Luas</h4>
        <p>L = 1/2 x D1 x D2</p>
    </blockquote>
    <blockquote class="materi-rumus-keliling">
        <h4>Rumus Keliling </h4>
        <p>K = S x 4</p>
    </blockquote>
</div>

<div class="materi-menghitung">
    <h4>Menghitung Luas dan Keliling</h4>
    <label for="sisi">Silahkan masukkan panjang sisi
        <input id="sisi" type="number">
    </label>
    <label for="diagonal1">Silahkan masukkan diagonal 1
        <input id="diagonal1" type="number">
    </label>
    <label for="diagonal2">Silahkan masukkan diagonal 2
        <input id="diagonal2" type="number">
    </label>
    <button type="submit" name="hitung-luas" onclick="luas()">Hitung Luas</button>
    <button type="submit" name="hitung-keliling" onclick="keliling()" >Hitung Keliling</button>
    <p id="materi-hasil-luas"></p>
    <p id="materi-hasil-keliling"></p>
</div>  

<script type="text/javascript">
    function luas() {
        var result = document.getElementById('materi-hasil-luas')
        var pj = new Number(document.getElementById('panjang').value)
        var lb = new Number(document.getElementById('lebar').value)
        var hasil = pj*lb
        result.innerHTML = "Luas : " + hasil
    }
    function keliling() {
        var result = document.getElementById('materi-hasil-keliling')
        var pj = new Number(document.getElementById('panjang').value)
        var lb = new Number(document.getElementById('lebar').value)
        var hasil = 2 * (pj + lb)
        result.innerHTML = "Keliling : " + hasil
    }
</script>