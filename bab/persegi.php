<p class="materi-definisi">Persegi adalalah bangun datar yang terbentuk dari empat buah sisi yang sama panjang dan empat sudut yang sama besar 90°.</p>

<div class="materi-sifat">
    <ol>
        <h4>Sifat</h4>
        <li>Memiliki dua pasang sisi yang sejajar dan sama panjang.</li>
        <li>Memiliki empat simetri lipat.</li>
        <li>Memiliki simetri putar tingkat empat.</li>
    </ol>
</div>

<div class="materi-rumus">
    <blockquote class="materi-rumus-luas">
        <h4>Rumus Luas</h4>
        <p>L = S<sup>2</sup></p>
    </blockquote>
    <blockquote class="materi-rumus-keliling">
        <h4>Rumus Keliling </h4>
        <p>K = 4 x S</p>
    </blockquote>
</div>

<div class="materi-menghitung">
    <h4>Menghitung Luas dan Keliling</h4>
    <label for="sisi">Panjang sisi
        <input id="sisi" type="text">
    </label>
    <button type="submit" name="hitung-luas" onclick="luas()">Hitung Luas</button>
    <button type="submit" name="hitung-keliling" onclick="keliling()" >Hitung Keliling</button>
</div>

<div class="materi-hasil">
    <p><b>Luas</b></p>
    <input type="text" id="materi-hasil-luas" readonly="readonly">
    <p><b>Keliling</b></p>
    <input type="text" id="materi-hasil-keliling" readonly="readonly">
</div>

<script type="text/javascript">
    function luas() {
        var result = document.getElementById('materi-hasil-luas')
        var sisi = new Number(document.getElementById('sisi').value)
        if (isNaN(sisi)) {
            result.value = "Maaf, input harus angka"
        } else {
            var hasil = sisi*sisi
            result.value = hasil
        }
    }
    function keliling() {
        var result = document.getElementById('materi-hasil-keliling')
        var sisi = new Number(document.getElementById('sisi').value)
        if (isNaN(sisi)) {
            result.value = "Maaf, input harus angka"
        } else {
            var hasil = sisi*4
            result.value = hasil
        }
    }
</script>