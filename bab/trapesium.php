<p class="materi-definisi">Trapesium adalah bangun datar dua dimensi tang dibentuk oleh 4 rusuk diantaranta saliung sejajar namun tidak sama panjang. </p>

<div class="materi-sifat">
    <ul class="materi-sifat">
        <h4>Sifat</h4>
        <li>Memiliki 4 sisi dan 4 titik sudut.</li>
        <li>Memiliki sepasang sisi yang sejajar tapi tidak ama panjang.</li>
        <li>Memiliki sudut di antara sisi sejajarnya sebesar 180°.</li>
    </ul>
</div>

<div class="materi-rumus">
    <blockquote class="materi-rumus-luas">
        <h4>Rumus Luas</h4>
        <p>L = 1/2 (AB + CD) x T </p>
    </blockquote>
    <blockquote class="materi-rumus-keliling">
        <h4>Rumus Keliling </h4>
        <p>K = AB + BC + CD + DA</p>
    </blockquote>
</div>

<div class="materi-menghitung">
    <h4>Menghitung Luas dan Keliling</h4>
    <label for="AB">Silahkan masukkan panjang AB
        <input id="AB" type="number">
    </label>
    <label for="BC">Silahkan masukkan panjang BC
        <input id="BC" type="number">
    </label>
    <label for="CD">Silahkan masukkan panjang CD
        <input id="CD" type="number">
    </label>
    <label for="DA">Silahkan masukkan panjang DA
        <input id="DA" type="number">
    </label>
    <label for="tinggi">Silahkan masukkan tinggi
        <input id="tinggi" type="number">
    </label>
    <button type="submit" name="hitung-luas" onclick="luas()">Hitung Luas</button>
    <button type="submit" name="hitung-keliling" onclick="keliling()" >Hitung Keliling</button>
</div>

<div class="materi-hasil">
    <p><b>Luas</b></p>
    <p id="materi-hasil-luas"></p>
    <p><b>Keliling</b></p>
    <p id="materi-hasil-keliling"></p>
</div>

<script type="text/javascript">
    function luas() {
        var result = document.getElementById('materi-hasil-luas')
        var AB = new Number(document.getElementById('AB').value)
        var BC = new Number(document.getElementById('BC').value)
        var CD = new Number(document.getElementById('CD').value)
        var DA = new Number(document.getElementById('DA').value)
        var tinggi = new Number(document.getElementById('tinggi').value)
        var hasil = 1/2 * (AB + CD) * tinggi
        result.innerHTML = "Luas : " + hasil
    }
    function keliling() {
        var result = document.getElementById('materi-hasil-keliling')
        var AB = new Number(document.getElementById('AB').value)
        var BC = new Number(document.getElementById('BC').value)
        var CD = new Number(document.getElementById('CD').value)
        var DA = new Number(document.getElementById('DA').value)
        var hasil = AB + BC + CD + DA
        result.innerHTML = "Keliling : " + hasil
    }
</script>