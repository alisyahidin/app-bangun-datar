<div class="materi-list">
    <ul>
        <li>
            <a href="materi.php?bab=persegi">
                <img src="bab/image/persegi.png" class="image-list persegi" alt="Persegi">
            </a>
        </li>
        <li>
            <a href="materi.php?bab=persegi-panjang">
                <img src="bab/image/persegi-panjang.png" class="image-list persegi-panjang" alt="Persegi">
            </a>
        </li>
        <li>
            <a href="materi.php?bab=segitiga">
                <img src="bab/image/segitiga.png" class="image-list segitiga" alt="Persegi">
            </a>
        </li>
        <li>
            <a href="materi.php?bab=jajar-genjang">
                <img src="bab/image/jajar-genjang.png" class="image-list jajar" alt="Persegi">
            </a>
        </li>
        <li>
            <a href="materi.php?bab=trapesium">
                <img src="bab/image/trapesium.png" class="image-list trapesium" alt="Persegi">
            </a>
        </li>
        <li>
            <a href="materi.php?bab=belah-ketupat">
                <img src="bab/image/belah-ketupat.png" class="image-list ketupat" alt="Persegi">
            </a>
        </li>
        <li>
            <a href="materi.php?bab=layang-layang">
                <img src="bab/image/layang-layang.png" class="image-list layang" alt="Persegi">
            </a>
        </li>
        <li>
            <a href="materi.php?bab=lingkaran">
                <img src="bab/image/lingkaran.png" class="image-list lingkaran" alt="Persegi">
            </a>
        </li>
    </ul>
</div>