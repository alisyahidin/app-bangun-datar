<?php
require "header.php";

$bab = $_REQUEST['bab'];

require "functions.php";
?>

<section class="materi">
    
    <img class="materi-gambar" src="bab/image/<?php echo $bab; ?>.png" alt="Persegi">

    <h3 class="materi-judul"><?php echo $bab; ?></h3>
    
    <?php
    if (isset($bab)) {
        if (!empty($bab)) {
            materi($bab);
        } else {
            echo "Silahkan pilih materi";
        }
    } else {
        // header("Location: ../index.php");
    }
    ?>
    
</section>

<?php require "footer.php"; ?>