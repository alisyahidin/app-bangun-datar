<?php

// Function Materi

function materi($args) {
    
    switch ($args) {
        case 'persegi':
            require "bab/$args.php";
            break;
        
        case 'persegi-panjang':
            require "bab/$args.php";
            break;
        
        case 'segitiga':
            require "bab/$args.php";
            break;
        
        case 'jajar-genjang':
            require "bab/$args.php";
            break;
        
        case 'trapesium':
            require "bab/$args.php";
            break;
        
        case 'belah-ketupat':
            require "bab/$args.php";
            break;
        
        case 'layang-layang':
            require "bab/$args.php";
            break;
        
        case 'lingkaran':
            require "bab/$args.php";
            break;
        
        default:
            require '404.php';
            break;
    }

}